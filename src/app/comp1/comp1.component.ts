import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-comp1',
  templateUrl: './comp1.component.html',
  styleUrls: ['./comp1.component.css']
})
export class Comp1Component implements OnInit {

  constructor() { }
  model: any = [];
  arrays = [];
  show:boolean=false;
  ngOnInit() {

  }
  addValue() {
    var d = new Date();
    // let time= this.arrays.push(d.toLocaleTimeString());
    // console.log(this.arrays);
    this.arrays.push(d.toLocaleTimeString());
    this.arrays.push(d.toLocaleString());
    this.arrays.push(this.model);
    console.log(this.arrays);
    this.show=true;
    this.model = [];
  }
}
